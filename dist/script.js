

function smoothScrollUp() {
  let smoothClass = document.querySelector("[data-up]");

  smoothClass.addEventListener("touchstart", function anima() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  })
}

function animateAccordion(name) {
  for (var i = 0; i < name.length; i++) {
    name[i].addEventListener("click", function activeToggle(e) {
      e.target.classList.toggle("opened");
    });
  }
}

function header() {
  let wrapper = document.querySelector('.header__wrapper');
  let wrapperTrue = document.querySelector('.header__wrapper.true');
  let cartTitle = document.querySelectorAll(".header__card-title");

  wrapperTrue.addEventListener("touchstart", function anima() {
    activeAnimation()
  })
  wrapper.addEventListener("touchstart", function anima() {
    activeAnimation()
  })

  function activeAnimation() {
    let contentPrincipal = document.querySelector('.content');
    let headerFix = document.querySelector('.header__wrapper-fix');
    let header = document.querySelector('.header');
    let burguer = document.querySelector('.header__burguer');
    let headerAccordion = document.querySelector('.header__accordion');

    contentPrincipal.classList.toggle("true");
    headerFix.classList.toggle("true");
    header.classList.toggle("true");
    headerAccordion.classList.toggle("true");
    wrapper.classList.toggle("true");
    burguer.classList.toggle("true");
  }
  animateAccordion(cartTitle);
}

function footer() {
  let cartTitleFooter = document.querySelectorAll(".footer__card-title");
  animateAccordion(cartTitleFooter);
}


function swiperCarroselHero() {
  let swiper = new Swiper('.swiper-container.hero', {
    slidesPerView: 1,
    spaceBetween: 0,
    centeredSlides: true,
    navigation: {
      nextEl: '.hero .swiper-button-next',
      prevEl: '.hero .swiper-button-prev',
    },
    pagination: {
      el: '.hero .swiper-pagination',
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
  });
}

function swiperCarroselBestllers() {
  let swiper = new Swiper('.swiper-container.bestsellers', {
    slidesPerView: 2,
    spaceBetween: 5,
    centeredSlides: false,
    navigation: {
      nextEl: '.bestsellers .swiper-button-next',
      prevEl: '.bestsellers .swiper-button-prev',
    },
  });
}

function swiperCarroselInstaShop() {
  let swiper = new Swiper('.swiper-container.instaShop', {
    slidesPerView: 2,
    spaceBetween: 5,
    centeredSlides: false,
    navigation: {
      nextEl: '.instaShop .swiper-button-next',
      prevEl: '.instaShop .swiper-button-prev',
    },
    pagination: {
      el: '.instaShop .swiper-pagination',
    },

  });
}

window.onload = function () {
  setTimeout(function () {
    header();
    footer();
    swiperCarroselHero();
    swiperCarroselBestllers();
    swiperCarroselInstaShop();
    smoothScrollUp();
  }, 1000);

};




