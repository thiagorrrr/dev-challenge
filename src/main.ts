import * as ko from 'knockout';
import { hero } from './components/hero/hero';
import { bestsellers } from './components/bestsellers/bestsellers';
import { instashop } from './components/instaShop/instashop';
import { cards } from './components/cards/cards';


import { header } from './components/header/header';
import { footer } from './components/footer/footer';

export default class ViewModel {
  private hero: void;
  private bestsellers: void;
  private instashop: void;
  private cards: void;
  private header: void;
  private footer: void;
  private apiData = ko.observableArray([]);

  constructor() {
    this.fetchData();
    this.hero = hero();
    this.bestsellers = bestsellers();
    this.instashop = instashop();
    this.cards = cards();
    this.header = header();
    this.footer = footer();

  }

  fetchData() {
    var that = this;
    var url = "http://localhost:8080/data";
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        that.apiData(JSON.parse(xhttp.responseText));
        console.log(that.apiData())
      }
    };
    xhttp.open("GET", url, true);
    xhttp.withCredentials = false;
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send();
  }
}

ko.applyBindings(new ViewModel());
