import * as ko from 'knockout';
export default class ViewModel {
    private apiData: any;


    constructor(params) {

        this.apiData = params.apiData;
    }
}


export function hero() {
    ko.components.register('ko-hero', {
        template: require('./hero.html'),
        viewModel: {
            createViewModel: (params, componentInfo) => new ViewModel(params)
        }
    });
}