import * as ko from 'knockout';
export default class ViewModel {
    private apiData: any;

    constructor(params) {
        this.apiData = params.apiData;
    }
}

export function instashop() {
    ko.components.register('ko-instashop', {
        template: require('./instashop.html'),
        viewModel: {
            createViewModel: (params, componentInfo) => new ViewModel(params)
        }
    });
}