
## Features
- Typescript
- Less
- Browserify
- Knockout custom elements
- CSS autoprefixing
- CSS reset and layout [sash-layout]
- Source Mapping
- Browser Sync
- Production asset minification
- Image optimization

# Usage

*Development:*
```shell
$ npm install
$ json-server --watch --port 8080 db.json bash1 terminal
$ npm start   bash2 terminal

And refresh page
```

*Clean dist folder:*
```shell
$ npm run clean
```

*Build:*
```shell
$ npm run build
```

*gh-pages Deployment:*
```shell
$ npm run deploy
```
